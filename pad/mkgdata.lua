local isshell = shell ~= nil
local argz = {...}

if not isshell and #argz < 1 then error("Please supply data directory as an argument") end 

local dir = (isshell and shell.dir() or argz[1])
local getfile

if isshell then
  getfile = shell.resolve
else
  getfile = function(wat) return fs.combine(dir, wat) end
end

local imgdir = getfile("imgdata")

local num = 1

local tbl = {}

print("Opening imgdata/bdata.txt")

local rhand = fs.open(fs.combine(imgdir, "bdata.txt"), "r")

local function getcolor (str)
  return colors[str] or colours[str]
end

local function hbit(n) local a = 0 while n > 0 do n = bit.brshift(n, 1) a = a + 1 end return a end

local ret1 = rhand.readLine()
local ret2 = rhand.readLine()

while ret1 and ret2 do
  print("Read char \"" .. tostring(ret1) .. "\" with color \"" .. tostring(ret2) .. "\"")
  table.insert(tbl, {ret1, ret2})
  ret1 = rhand.readLine()
  ret2 = rhand.readLine()
end

rhand.close()

local whand = fs.open(getfile("gdata"), "wb")

whand.write(#tbl)
print(#tbl .. " blockies found")
for k,v in pairs(tbl) do
  whand.write(string.byte(v[1]))
  whand.write(hbit(getcolor(v[2])) - 1)
end

tbl = {}

num = 1

while true do
  local fn = fs.combine(imgdir, tostring(num))
  if not fs.exists(fn) or fs.isDir(fn) or not fs.exists(fn .. ".txt") or fs.isDir(fn .. ".txt") then num = num - 1 break end
  print("Found \"" .. num .. "\" with \"" .. num .. ".txt\"")
  table.insert(tbl, fn)
  num = num + 1
end

print("Total " .. num .. " creatures added")

local function wint(x)
  local b4=x%256 x=(x-x%256)/256
  local b3=x%256 x=(x-x%256)/256
  local b2=x%256 x=(x-x%256)/256
  local b1=x%256 x=(x-x%256)/256
  whand.write(b1)
  whand.write(b2)
  whand.write(b3)
  whand.write(b4)
  return x
end

local function wstr(str)
  whand.write(string.len(str))
  for i in str:gmatch(".") do
    whand.write(string.byte(i))
  end
end

wint(num)

for i = 1, num do
  print("Adding " .. i .. "/" .. num)
  local t = tbl[i]
  rhand = fs.open(t, "rb")
  
  ret1 = rhand.read()
  while ret1 do
    whand.write(ret1)
    ret1 = rhand.read()
  end
  rhand.close()
  rhand = fs.open(t .. ".txt", "r")
  wstr(rhand.readLine())
  whand.write(tonumber(rhand.readLine()))
  wint(tonumber(rhand.readLine()))
  wint(tonumber(rhand.readLine()))
  wint(tonumber(rhand.readLine()))
  whand.write(tonumber(rhand.readLine()))
  whand.write(tonumber(rhand.readLine()))
  whand.write(tonumber(rhand.readLine()))
  rhand.close()
  whand.flush()
end

whand.flush()
whand.close()

print("Done! ^^")
return true
