local isshell = shell ~= nil
local argz = {...}

if not isshell and #argz < 1 then error("Please supply data directory as an argument") end

local isdev = isshell and (#argz == 1 and argz[1] == "dev") or (#argz == 2 and argz[2] == "dev")

local dir = (isshell and shell.dir() or argz[1])
local getfile

if isshell then
  getfile = shell.resolve
else
  getfile = function(wat) return fs.combine(dir, wat) end
end

local redrva

--[HARDCODED LOCALS]--

local revcol = {[1] = 32768, [2] = 16384, [4] = 8192, [8] = 4096, [16] = 2048, [32] = 1024, [64] = 512, [128] = 256, [256] = 128, [512] = 64, [1024] = 32, [2048] = 16, [4096] = 8, [8192] = 4, [16384] = 2, [32768] = 1}

local inf = math.pow(2, 255)

--Normal screen
local minw1 = 24
local minh1 = 16
--Pocket (rotated) screen
local minw2 = 16
local minh2 = 20

local isrotated = false

--[HARDCODED LOCALS]--

local w, h = term.getSize()

if pocket then
  isrotated = true
else
  isrotated = w < h
end

local function checkRotate()
  if isrotated and (w < minw2 or h < minh2) or (w < minw1 or h < minh1) then return false end
  return true
end

--[GAME DATA]--

local saev = {}
local gdata = {}

gdata.colors = {blocky = {colors.red, colors.green, colors.blue, colors.magenta, colors.yellow, colors.pink}}
gdata.icon = {blocky = {"^", "S", "W", "V", "*", "H"}}

local function ldgdata()
  print("Loading game data")
  local rhand = fs.open(getfile("gdata"), "rb")
  
  local function rint()
    local b1, b2, b3, b4 = rhand.read(), rhand.read(), rhand.read(), rhand.read()
    return b1*16777216 + b2*65536 + b3*256 + b4
  end
  
  local function rstr()
    local buf = {}
    local cnt = rhand.read()
    for i = 1, cnt do
      table.insert(buf, string.char(rhand.read()))
    end
    return table.concat(buf, "")
  end
  
  gdata.colors = {}
  gdata.colors.blocky = {}
  gdata.icon = {}
  gdata.icon.blocky = {}
  gdata.monster = {}
  
  local cnt = rhand.read()
  print("Loading " .. cnt .. " blockies")
  for i = 1, cnt do
    gdata.icon.blocky[i] = string.char(rhand.read())
    gdata.colors.blocky[i] = math.pow(2, rhand.read())
    print("Blocky \"" .. gdata.icon.blocky[i] .. "\" is " .. tostring(gdata.colors.blocky[i]))
  end
  
  cnt = rint()
  print("Loading " .. cnt .. " creatures")
  for i = 1, cnt do
    print("Loading " .. i .. "/" .. cnt)
    gdata.monster[i] = {}
    local m = gdata.monster[i]
    local texture = {}
    local tw = rhand.read()
    local th = rhand.read()
    for i = 1, tw do
      texture[i] = {}
      for j = 1, th do
        texture[i][j] = {}
        texture[i][j].bg = math.pow(2, rhand.read())
        texture[i][j].fg = math.pow(2, rhand.read())
        texture[i][j].txt = string.char(rhand.read())
      end
    end
    
    m.texture = texture
    m.tw = th
    m.th = tw
    m.name = rstr()
    m.t = rhand.read()
    m.heal = rint()
    m.atk = rint()
    m.def = rint()
    m.natk = rhand.read()
    m.rar = rhand.read()
    m.cst = rhand.read()
  end
  
  rhand.close()
  print("Successfuly loaded everything ^^")
end

local function getsaev(n)
  if not fs.exists(getfile("saev")) then
    fs.makeDir(getfile("saev"))
  end
  local feil = getfile(fs.combine("saev", n))
  if not fs.exists(feil) then
    local whand = fs.open(feil, "wb")
    
    local function wint(x)
      local b4=x%256 x=(x-x%256)/256
      local b3=x%256 x=(x-x%256)/256
      local b2=x%256 x=(x-x%256)/256
      local b1=x%256 x=(x-x%256)/256
      whand.write(b1)
      whand.write(b2)
      whand.write(b3)
      whand.write(b4)
      return x
    end
    
    local function wlong(x)
      wint(wint(x))
    end
    
    local function wstr(str)
      whand.write(string.len(str))
      for i in str:gmatch(".") do
        whand.write(string.byte(i))
      end
    end
    
    
    
  end
end

--[GAME DATA]--

--[BASIC SCREEN]--

local screens = {}
local screen = 1
local bgcolor, fgcolor = colors.black, colors.white

local function setbg(c)
  if not c or type(c) ~= "number" or c < 1 or c > 65535 then error(type(c) .. ": \"" .. tostring(c) .. "\"") end
  bgcolor = c
  term.setBackgroundColor(c)
end

local function setfg(c)
  if not c or type(c) ~= "number" or c < 1 or c > 65535 then error(type(c) .. ": \"" .. tostring(c) .. "\"") end
  fgcolor = c
  term.setTextColor(c)
end

local function setcur(x, y)
  term.setCursorPos(x, y)
end

local function getcur()
  return term.getCursorPos()
end

--[BASIC SCREEN]--

--[GAME LOGIC]--

local blockyw = 6
local blockyh = 5
local blocky = {}
local numvar = #gdata.icon.blocky

local totalinc = 0
local score = 0
local pty = {}
local emy = {}

local function repopulate(force)
  for i = 1, (blockyw * blockyh) do
    if not blocky[i] or force then
      blocky[i] = math.random(1, numvar)
    end
  end
end

local function match()
  local tbl = {}
  for i = 0, blockyh - 1 do
    local offs = (i * blockyw)
    local prev = -1
    local now
    local cnt = 1
    for j = 1, blockyw do      
      now = blocky[offs + j]
      if now == prev then
        cnt = cnt + 1
      else
        if cnt >= 3 then
          table.insert(tbl, {prev, offs + j - cnt, cnt, false})
        end
        cnt = 1
      end
      prev = now
    end
    if cnt >= 3 then
      table.insert(tbl, {prev, ((i + 1) * blockyw) - cnt + 1, cnt, false})
      cnt = 1
    end
  end
  for i = 1, blockyw do
    local prev = -1
    local now
    local cnt = 1
    for j = 0, blockyh - 1 do      
      now = blocky[(j * blockyw) + i]
      if now == prev then
        cnt = cnt + 1
      else
        if cnt >= 3 then
          table.insert(tbl, {prev, (blockyw * (j - cnt)) + i, cnt, true})
        end
        cnt = 1
      end
      prev = now
    end
    if cnt >= 3 then
      table.insert(tbl, {prev,  (blockyw * (blockyh - cnt)) + i, cnt, true})
      cnt = 1
    end
  end
  if #tbl == 0 then return nil end
  return tbl
end

local function fall(haztop)
  local moved = false
  for i = blockyw, 1, -1 do
    for j = blockyh - 1, (haztop and 0 or 1), -1 do
      if not blocky[i + (j * blockyw)] and blocky[i + ((j - 1) * blockyw)] then
        moved = true
        blocky[i + (j * blockyw)] = blocky[i + ((j - 1) * blockyw)]
        blocky[i + ((j - 1) * blockyw)] = nil
      end
    end
  end
  return moved
end

local function destroy(tbl)
  local ret = {}
  for k,v in pairs(tbl) do
    if v[4] then
      for i = 0, v[3] - 1 do
        blocky[v[2] + (i * blockyw)] = nil
      end
    else
      for i = 0, v[3] - 1 do
        blocky[v[2] + i] = nil
      end
    end
    if ret[v[1]] == nil then
      ret[v[1]] = {cnt = 0, num = 0}
    end
    ret[v[1]].num = ret[v[1]].num + v[3]
    ret[v[1]].cnt = ret[v[1]].cnt + 1
  end
  return ret
end

local function switch(i1, i2)
  local tmp = blocky[i1]
  blocky[i1] = blocky[i2]
  blocky[i2] = tmp
end

local function getAtk(t)
  local ret = 0
  for k,v in pairs(pty) do
    if gdata.monster[v.id].t == t then
      ret = ret + gdata.monster[v.id].atk
    end
  end
  return ret
end

--GAME GRAPHICS
local function updateScore(sco)
  if not sco then sco = 0 end
  score = score + sco
  local ox, oy = getcur()
  local scorestr = "Score: " .. tostring(score)
  setcur(w - #scorestr - 2, 3)
  write(scorestr)
  setcur(ox, oy)
end

local drawHealth, drawEnemy, newgame, setScreen

local function drgend(destroyd)
  local bgcol = bgcolor
  local atkdmg = 0
  local recv = 0
  if destroyd and #destroyd ~= 0 then
    local destcnt = #destroyd
    local bonus = 1
    for k,v in pairs(destroyd) do
      for it, a in pairs(v) do
        local an = getAtk(it) * (a.num - (a.cnt * 2)) * bonus
        updateScore(an)
        atkdmg = atkdmg + an
      end
      bonus = bonus + 0.5
    end
  end
  updateScore()
  drawEnemy()
  drawHealth()
  local function drawHeal(x, y, wi, num, mx)
    local oldbg = bgcolor
    local cx = math.floor(x + (wi / 2) + 0.5)
    if mx == 0 then mx = 1 end
    local wid = math.floor((num / mx) * wi)
    local n = math.floor((#healColors * ( wid / wi )) + 0.5)
    setbg(healColors[n])
    setfg(revcol[healColors[n]])
    setcur(cx - (wid / 2), y)
    write(string.rep(" ", wid))
    local txt = num .. "/" .. mx
    setcur(cx - (string.len(txt) / 2) + 1, y)
    write(txt)
    setbg(oldbg)
  end
  local nume = 0
  local restoration = {}
  for k,v in pairs(emy) do
    restoration[k] = v
    nume = nume + 1
  end
  local finala = math.floor(atkdmg / nume)
  for nuu,ee in pairs(restoration) do
    local moo = gdata.monster[ee.id]
    ee.heal = ee.heal - finala
    if ee.heal <= 0 then
      emy[nuu] = nil
      ee.tick = 1000
      redrva()
    end
    ee.tick = ee.tick - 1
    if ee.tick < 0 then
      recv = recv + moo.atk
      emy[nuu] = nil
      redrva()
      local xpso = math.floor((w / 2) - (moo.tw / 2))
      local xyso = h - 3 - moo.th
      for k,v in pairs(moo.texture) do
        setcur(xpso, xyso + k)
        for a, i in pairs(v) do
          setbg(i.bg == inf and bgcol or i.bg)
          setfg(i.fg)
          write(i.txt)
        end
        --drawHeal(start + offs, sy + moo.th + centery + 1, moo.tw, ee.heal, moo.heal)
      end
      term.scroll(-1)
      --for i = 1, 3 do
        term.scroll(2)
        sleep(0.2)
        term.scroll(-2)
        sleep(0.2)
      --end
      emy[nuu] = ee
      emy[nuu].tick = moo.natk
      redrva()
    end
  end
  restoration = {}
  for k,v in pairs(pty) do
    restoration[k] = v
    nume = nume + 1
  end
  for k,v in pairs(restoration) do
    if v.heal <= recv then
      recv = recv - v.heal
      v.heal = 0
      pty[k]= nil
      redrva()
    else
      v.heal = v.heal - recv
      recv = 0
      redrva()
      break
    end
  end
  nume = 0
  local numa = 0
  for k,v in pairs(emy) do
    nume = nume + 1
  end
  for k,v in pairs(pty) do
    numa = numa + 1
  end
  if nume == 0 or numa == 0 then
    redrva()
    local srea = (nume == 0 and (numa == 0 and "Lol, everyone fell :P" or "Lol, you won!") or "You lost :(")
    local sreb = "Stay tuned for updates!"
    setcur((w / 2) - (#srea / 2) , h / 2)
    write(srea)
    setcur((w / 2) - (#sreb / 2) , (h / 2) + 1)
    print(sreb)
    print("Press a key to return to the main menu")
    newgame()
    os.pullEvent("key")
    if numa == 0 then setfg(colors.gray) end
    setScreen(0)
  end
end


--[GAME LOGIC]--

--[GAME GRAPHICS]--

local emptyline, centerx, beginx, beginy

local function doresize()
  centerx = w / 2
  beginx = math.floor(centerx - (blockyw / 2))
  beginy = math.floor(h - blockyh - 3)
  emptyline = string.rep(" ", blockyw)
end

local function clearTable()
  local ox, oy = getcur()
  for i = 1, blockyh do
    setcur(beginx, beginy + i)
    write(emptyline)
  end
  setcur(ox, oy)
end

local function drawTable()
  local oldbg = bgcolor
  local oldfg = fgcolor
  local cols = gdata.colors.blocky
  local icon = gdata.icon.blocky
  for i = 1, blockyh do
    local offs = ((i - 1) * blockyw)
    setcur(beginx, beginy + i)
    for j = 1, blockyw do
      local blk = blocky[offs + j]
      if blk == nil then
        setbg(colors.black)
        write(" ")
      else
        setbg(cols[blk])
        setfg(revcol[cols[blk]])
        write(icon[blk])
      end
    end
  end
  setbg(oldbg)
  setfg(oldfg)
end

--[GAME GRAPHICS]--

--[[

0: Main menu
1: 

--]]

local guiTopline, guiBottomline, bigborder = "", "", true

local function drawScreen()
  local tbl = screens[screen]
  if not tbl then return end
  if screen < 0 then tbl:draw() return end
  for x,vo in pairs(tbl) do
    for y, v in pairs(vo) do
      setcur(x, y)
      if v.type == "label" then
        if v.color then
          local oldfg = fgcolor
          setfg(v.color)
          write(v.text)
          setfg(oldfg)
        else
          write(v.text)
        end
      elseif v.type == "minbtn" then
        local oldfg, oldbg = fgcolor, bgcolor
        if v.color then
          setfg(v.color[1])
          setbg(v.color[2])
        else
          setfg(colors.white)
          setbg(colors.cyan)
        end
        write(v.text)
        setfg(oldfg)
        setbg(oldbg)
      else
        error("Invalid type: " .. tostring(v.type))
      end
    end
  end
end

local function drawBorder()
  local cx, cy = getcur()
  setcur(1,1)
  print(guiTopline)
  for i = 2, w - 1 do
    setcur(1, i)
    write("|")
    setcur(w, i)
    write("|")
  end
  setcur(1, h)
  write(guiBottomline)
  setcur(cx, cy)
end

local healColors = { colors.red, colors.orange, colors.green, colors.blue }
healColors[0] = colors.black

drawHealth = function()
  local oldbg = bgcolor
  local hm = 0
  local hn = 0
  for k,v in pairs(pty) do
    hm = hm + gdata.monster[v.id].heal
    hn = hn + v.heal
  end
  if hm == 0 then hm = 1 end
  local wid = (hn / hm) * (w - 2)
  local n = math.floor((#healColors * ( wid / (w - 2))) + 0.5)
  setbg(healColors[n])
  setfg(revcol[healColors[n]])
  setcur(centerx - (wid / 2) + 1, h - 1)
  write(string.rep(" ", wid))
  local txt = hn .. "/" .. hm
  setcur(centerx - (string.len(txt) / 2), h - 1)
  write(txt)
  setbg(oldbg)
end

setScreen = function(n)
  screen = n
  term.clear()
  drawBorder()
  drawScreen()
end

local function resize()
  w, h = term.getSize()
  if pocket then
    isrotated = true
  else
    isrotated = w < h
  end
  if not checkRotate() then error("Your screen is too small. Get a bit bigger one :P") end
  
  doresize()
  
  guiTopline = table.concat({"/", string.rep((bigborder and "=" or "-"), centerx - 5), "[CCPad]", string.rep((bigborder and "=" or "-"), centerx - 3), "\\"}, "")
  guiBottomline = table.concat({"\\", string.rep((bigborder and "=" or "-"), w - 2), "/"}, "")
  
  if isrotated then
    error("No landscape mode pls")
  else
    screens[0] =
    {
      [3] =
      {
        [3] =
        {
          type = "label",
          text = "Main menu"
        },
        [5] =
        {
          type = "minbtn",
          text = "*",
          func = setScreen,
          argz = {-1} --wat
        },
        [7] =
        {
          type = "minbtn",
          text = "*",
          func = setScreen,
          argz = {1} --wat
        },
        [8] =
        {
          type = "minbtn",
          text = "*",
          func = setScreen,
          argz = {3} --wat
        }
      },
      [5] =
      {
        [5] =
        {
          type = "label",
          text = "New game" --wat
        },
        [7] =
        {
          type = "label",
          text = "Load save" --wat
        },
        [8] =
        {
          type = "label",
          text = "Manage saves" --wat
        }
      }
    }
  end
end

drawEnemy = function()
  local bgcol = bgcolor
  local padding = 2
  local totalw = 0
  local start = 0
  local offs = 0
  local centery = math.floor(((h - 3) / 2) - (beginy / 2) + 1)
  for k,v in pairs(emy) do
    totalw = totalw + gdata.monster[v.id].tw + padding
  end
  start = math.floor(centerx - (totalw / 2) + 1)
  
  local currwid = 0
  
  local function drawHeal(x, y, wi, num, mx)
    local oldbg = bgcolor
    local cx = math.floor(x + (wi / 2) + 0.5)
    if mx == 0 then mx = 1 end
    local wid = math.floor((num / mx) * wi)
    local n = math.floor((#healColors * ( wid / wi )) + 0.5)
    setbg(healColors[n])
    setfg(revcol[healColors[n]])
    setcur(cx - (wid / 2), y)
    write(string.rep(" ", wid))
    local txt = num .. "/" .. mx
    setcur(cx - (string.len(txt) / 2) + 1, y)
    write(txt)
    setbg(oldbg)
  end
  
  for nuu,ee in pairs(emy) do
    local moo = gdata.monster[ee.id]
    local sy = math.floor(centery - (moo.th / 2))
    setcur(start + offs, sy + centery)
    setbg(bgcol)
    setfg(ee.tick < 4 and colors.red or colors.green)
    write("["..ee.tick.."]")
    for k,v in pairs(moo.texture) do
      setcur(start + offs, sy + k + centery)
      for a, i in pairs(v) do
        setbg(i.bg == inf and bgcol or i.bg)
        setfg(i.fg)
        write(i.txt)
      end
      drawHeal(start + offs, sy + moo.th + centery + 1, moo.tw, ee.heal, moo.heal)
    end
    offs = offs + moo.tw + padding
  end
  setbg(bgcol)
end

local function touchFile(wat)
  local feil = getfile(wat)
  if not fs.exists(feil) then
    local hand = fs.open(feil, "wb")
    hand.flush()
    hand.close()
  end
end

redrva = function()
  setfg(colors.white)
  setbg(colors.black)
  term.setCursorPos(1, 1)
  term.clear()
  drawBorder()
  drawScreen()
end

ldgdata()
term.clear()
resize()
drawBorder()
setScreen(0)

--it has a meaning that this function is on the 666th line, thus should it be considered as evil >:D
newgame = function()
  emy = {}
  pty = {}
  repopulate(true)
  matret = match()
  while matret do
    destroy(matret)
    while fall() do sleep() end
    repopulate()
    matret = match()
  end
  local oldseed = math.random(1, math.pow(2, 31) - 1)
  --math.randomseed(56)
  local cnt = math.random(2, 4)
  for i = 1, cnt do
    local t = {}
    t.id = math.random(1, #gdata.monster)
    t.heal = gdata.monster[t.id].heal
    t.tick = gdata.monster[t.id].natk
    table.insert(emy, t)
  end
  cnt = math.random(3, 5)
  for i = 1, cnt do
    local t = {}
    t.id = math.random(1, #gdata.monster)
    t.heal = gdata.monster[t.id].heal
    table.insert(pty, t)
  end
  math.randomseed(oldseed)
  math.random(1, 1)
end

local matret
local distret
local ndrg = 0
local si

screens[-1] =
{
  draw = function()
    if isdev then
      term.setCursorPos(1,1)
      print("*Repopulate")
      print("*Match")
      print("*Destroy")
      print("*Fall")
      print("*Draw")
      print("*Clear")
      print(" ")
      print("*Force redraw")
      print("*Print matches")
    end
    drawTable()
    updateScore()
    drawEnemy()
    drawHealth()
  end,
  
  drg = function(selfe, b, bx, by)
    if bx >= beginx and bx <= beginx + blockyw - 1 and by > beginy and by <= beginy + blockyh then
      local px = bx - beginx + 1
      local py = by - beginy - 1
      local index = (py * blockyw) + px
      if not si then return end
      switch(index, si)
      ndrg = ndrg + 1
      matret = match()
      drawTable()
      --if not matret then si = index return end
      local res = {}
      if matret then
        ndrg = 0
        while matret do
          table.insert(res, destroy(matret))
          drawTable()
          while fall() do drawTable() sleep(0.2) end
          repopulate()
          drawTable()
          matret = match()
        end
        si = nil
      elseif ndrg > 5 then
        ndrg = 0
        si = nil
      else
        si = index
        drawTable()
        return
      end
      drawTable()
      drgend(res)
    end
  end,
  
  click = function(selfe, b, bx, by)
    if bx == 1 and isdev then
      if by == 1 then
        repopulate(b ~= 1)
        drawTable()
      elseif by == 2 then
        matret = match()
        drawTable()
      elseif by == 3 then
        if matret then
          destroy(matret)
        end
        drawTable()
      elseif by == 4 then
        if b == 1 then
          fall()
        else
          while fall() do drawTable() sleep(0.5) end
        end
        drawTable()
      elseif by == 5 then
        drawTable()
      elseif by == 6 then
        clearTable()
      elseif by == 8 then
        setfg(colors.white)
        setbg(colors.black)
        term.setCursorPos(1, 1)
        term.clear()
        drawBorder()
        drawScreen()
      elseif by == 9 then
        term.setCursorPos(2, 2)
        if matret then
          print(textutils.serialize(matret))
          os.pullEvent("key")
        end
        drawScreen()
      end
    elseif bx == w and by == 1 then
      newgame()
      redrva()
    elseif bx >= beginx and bx <= beginx + blockyw - 1 and by > beginy and by <= beginy + blockyh then
      local px = bx - beginx + 1
      local py = by - beginy - 1
      local index = (py * blockyw) + px
      --[[if si then
        if index == si then
          si = nil
        else
          switch(index, si)
          drawTable()
          matret = match()
          while matret do
            if not matret then return end
            destroy(matret)
            drawTable()
            while fall() do drawTable() sleep(0.2) end
            repopulate()
            drawTable()
            matret = match()
          end
          drawTable()
          si = nil
        end
      else
        si = index
      end
      ]]--
      if si and drgd ~= 0 then
        drgend()
        si = nil
      else
        si = index
        drgd = 0
      end
    end
  end
}

newgame()

while true do
  local evt = {coroutine.yield()}
  local e = evt[1]
  local edata = {unpack(evt, 2)}
  
  if e == "terminate" then
    print("IDKLol, add saving code here")
    error("Terminated")
  elseif screen >= 0 then
    if e == "mouse_click" then
      local scr = screens[screen]
      if scr[edata[2]] and scr[edata[2]][edata[3]] and scr[edata[2]][edata[3]].func then
        local wat = scr[edata[2]][edata[3]]
        if wat.argz then
          wat.func(unpack(wat.argz))
        else
          wat.func()
        end
      end
    elseif e == "monitor_resize" or e == "term_resize" then
      term.clear()
      resize()
      drawBorder()
      drawScreen()
    end
  else
    if e == "mouse_click" then
      screens[screen]:click(unpack(edata))
    elseif e == "mouse_drag" then
      screens[screen]:drg(unpack(edata))
    elseif e == "monitor_resize" or e == "term_resize" then
      term.clear()
      resize()
      drawBorder()
      drawScreen()
    end
  end
end
